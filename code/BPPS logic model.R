

library(DiagrammeR)


# logic model -------------------------------------------------------------

grViz("
      digraph{
      
      graph[layout=dot]
      rankdir=BT
      fontcolor='yellow'
      
      node[shape=box, color='MistyRose', style='filled',fontsize=16, alpha=.2]
      A[label='\nUSAID activities\n\n', fontcolor='Maroon']
      B[label='USAID media\nmessaging', fontcolor='Maroon']
      C[label='USAID\nbranding', fontcolor='Maroon']
      D[label='Unique info\nabout USAID', fontcolor='Maroon']
      E[label='Government\nactivities', fontcolor='Maroon']
      F[label='Other donor\nactivities', fontcolor='Maroon']
      
      node[shape=box, color='lightblue1', style='filled', fontsize=16]
      G[label='USAID perception', fontcolor='MidnightBlue']
      H[label='GIRoA perception', fontcolor='MidnightBlue']
      
      edge[penwidth=1.5, color='darkblue']
      {E F A} -> H [color='DimGray']
      {B C D A} -> G [color='DimGray']
      #H -> G [color='DimGray']
      #G -> H [color='DimGray']
      H -> G [ color='DimGray']
      }
      ")


# node[shape=box, color='cadetblue1',style='filled', fontsize=16]
# DA[label='Not aware,\ninfo treatment,\nUSAID message']
# DB[label='Not aware,\ninfo treatment,\nno USAID message']
# I[label='Noticed USAID\nactivity']
# J[label='Participated in\nUSAID activity']


# variations in BPPS

grViz("
      digraph{
      
      graph[layout=dot]
      
      node[shape=box, color='khaki1', style='filled',fontsize=16]
      A[label='Aware of USAID']
      
      node[shape=box, color='darkseagreen1', style='filled', fontsize=16]
      D[label='Not aware,\ninformation treatment']
      E[label='Not aware,\nno information treatment']
      F[label='Aware,\nno information treatment']
      G[label='Aware,\ninformation treatment']
      
      node[shape=box, color='cadetblue1',style='filled', fontsize=16]
      DA[label='Not aware,\ninfo treatment,\nUSAID message']
      DB[label='Not aware,\ninfo treatment,\nno USAID message']
      I[label='Noticed USAID\nactivity']
      J[label='Participated in\nUSAID activity']
      
      edge[penwidth=1.5]
      A -> {D E F G}
      D -> {DA DB}
      DA -> {H I J}
      E -> {H I J}
      F -> {H I J}
      G -> {H I J}
      }
      ")




grViz("
      digraph{
      
      graph[layout=dot]
      
      node[shape=box, color='khaki1', style='filled',fontsize=16]
      A[label='EASES BS Women\nHH (Client).dta']
      B[label='EASEBS Female-HH\nrenamed (14 Nov 2019).dta']
      C[label='EASEBS Female\nHH clean.dta']
      
      node[shape=box, color='darkseagreen1', style='filled', fontsize=16]
      D[label='rename and new label for variables\nFemale HH (14 Nov 2019).do']
      E[label='Female HH\nfrequency prepare.do']
      F[label='female HH\nfrequencies.do']
      
      node[shape=box, color='cadetblue1',style='filled', fontsize=16]
      G[label='EASEBS female hh\ndescriptives - by location treat.log']
      
      edge[penwidth=1.5]
      A->{B E D}
      D->B
      D->E
      E->C
      E->F
      F->G
      }
      ")





# dag -----------------------------------------------------------------

library(dagitty)

b <- dagitty('dag {
             A[pos="0,1"]
             T[exposure, pos="1,1"]
             Y[outcome, pos="2,2"]
             
             T -> Y
             A -> Y
             A -> T
}
             ')

plot(b)
impliedConditionalIndependencies(b)

g1 <- dagitty('dag {
    USAID_Influence [pos="0,1"]
    USAID_Perception [pos="1.7,1"]
    GIRoA_Activities [pos=".5,-.7"]
    Stability [pos="1.3,-.2"]
    Security [pos=".8,1.5"]
    COVID [pos="1.2,2"]
    Other_Donors [pos="1,-1.2"]
    
    USAID_Influence -> {USAID_Perception COVID}
    USAID_Influence -> GIRoA_Activities -> Stability -> USAID_Perception
    Security <-> USAID_Influence 
    Security -> USAID_Perception
    COVID -> USAID_Influence
    COVID -> USAID_Perception
    Stability -> USAID_Influence
    Other_Donors -> GIRoA_Activities
    Other_Donors -> Stability
    Security -> Stability
    COVID -> {GIRoA_Activities Stability USAID_Perception}
    GIRoA_Activities <-> COVID

}')
plot(g1)


paths(g1, "USAID_Influence","USAID_Perception", directed=T)
impliedConditionalIndependencies(g1)

?impliedConditionalIndependencies
?adjustmentSets

adjustmentSets(g1, exposure="USAID_Influence", outcome="USAID_Perception", type="minimal", effect="direct")


x <- dagitty('dag {
    USAID_Influence [exposure, pos="0,1"]
    USAID_Perception [outcome, pos="2,1"]
    GIRoA_Activities [pos="2,1"]
    GIRoA_Effectiveness [pos="1,0"]
    Security -> USAID_Influence
    Security -> USAID_Perception
    COVID [pos="1,3"]
   
    USAID_Influence -> USAID_Perception
    USAID_Influence -> GIRoA_Activities
    COVID -> USAID_Influence
    COVID -> USAID_Perception
    GIRoA_Activities -> GIRoA_Effectiveness
    Security -> USAID_Influence
    Security -> USAID_Perception
    
}')
plot(x)



?dagitty


h <- dagitty('dag {
    X [pos="0,1"]
    Y [pos="1,1"]
    Z [pos="2,1"]
    W [pos="1,0"]
    T [pos="4,5"]
    
    X -> Y -> Z -> T
    X -> W -> Y -> T
    W -> Z
}')
plot(h)

impliedConditionalIndependencies(h)







g2 <- dagitty('dag {
    USAID_Influence [pos="0,1"]
    USAID_Perception [pos="1.7,1"]
    GIRoA_Activities [pos=".5,-.7"]
    Stability [pos="1.3,-.2"]
    Security [pos="1.2,1.7"]
    COVID [pos=".5,2"]
    Other_Donors [pos="1,-1.2"]
    
    USAID_Influence -> USAID_Perception
    USAID_Influence -> GIRoA_Activities -> Stability -> USAID_Perception
    Security -> USAID_Influence 
    Security -> USAID_Perception
    COVID -> USAID_Influence
    COVID -> USAID_Perception
    Stability -> USAID_Influence
    Other_Donors -> {GIRoA_Activities Stability}
    Security -> Stability
    COVID -> Stability

}')
plot(g2)


paths(g2, "USAID_Influence","USAID_Perception", directed=T)
impliedConditionalIndependencies(g2)

?impliedConditionalIndependencies
?adjustmentSets

adjustmentSets(g2, exposure="USAID_Influence", outcome="USAID_Perception", effect="direct")


# all USG ----

grViz("
      digraph{
      
      graph[layout=dot]
      rankdir=BT
      fontcolor='yellow'
      
      node[shape=box, color='MistyRose', style='filled',fontsize=16, alpha=.2]
      A[label='USG', fontcolor='Maroon']
      B[label='USAID media\nmessaging', fontcolor='Maroon']
      C[label='USAID\nbranding', fontcolor='Maroon']
      D[label='Unique info\nabout USAID', fontcolor='Maroon']
      E[label='Government\nactivities', fontcolor='Maroon']
      F[label='Other donor\nactivities', fontcolor='Maroon']
      
      node[shape=box, color='lightblue1', style='filled', fontsize=16]
      G[label='USAID perception', fontcolor='MidnightBlue']
      H[label='GIRoA perception', fontcolor='MidnightBlue']
      
      edge[penwidth=1.5, color='darkblue']
      {E F A} -> H [color='DimGray']
      {B C D A} -> G [color='DimGray']
      #H -> G [color='DimGray']
      #G -> H [color='DimGray']
      H -> G [ color='DimGray']
      }
      ")




