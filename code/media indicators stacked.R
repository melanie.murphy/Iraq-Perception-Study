# Media indicators stacked

actSurv <- merg %>%
  group_by(Survey) %>%
  summarize(value=mean(USAID_activity)) %>%
  mutate(Measure="Aware of USAID activity")

actSurv

mesSurv <- merg %>%
  group_by(Survey) %>%
  summarize(value=mean(USAID_message)) %>%
  mutate(Measure="Exposed to USAID media message")

mesSurv

names(merg)

percepmes_Surv <- merg %>%
  group_by(Survey) %>%
  summarize(value = mean(percept_message, na.rm=T)) %>%
  mutate(Measure="Perception of USAID media message")

percepmes_Surv

particSurv <- merg %>%
  group_by(Survey) %>%
  summarize(value=mean(USAID_training)) %>%
  mutate(Measure="Participated in USAID activity")

particSurv

tv <- merg2 %>%
  group_by(Survey) %>%
  summarize(value=mean(daily_hrs_tv)) %>%
  mutate(Measure="Daily hours television consumption")

tv

rad <- merg2 %>%
  group_by(Survey) %>%
  summarize(value=mean(daily_hrs_radio)) %>%
  mutate(Measure="Daily hours radio consumption")

rad

soc_use <- merg %>%
  group_by(Survey) %>%
  summarize(value = mean(social_media_engage, na.rm=T)) %>%
  mutate(Measure="Social media user")

soc_use

out <- do.call(rbind,
               list(mesSurv,
                    percepmes_Surv,
                    actSurv,
                    particSurv,
                    tv,
                    rad,
                    soc_use)) %>%
  select(1,3,2)

out

write_csv(out, "output/tables/merged/media indicators stacked.csv")

?libPaths

?.libpaths
.libPaths()

