
byAge <- dat %>%
  group_by(age) %>%
  summarize(aware=mean(aware,na.rm=T))

byAge

ggplot(byAge, aes(age, aware, group=1)) +
  geom_point() +
  stat_smooth(span=.99)


ggplot(filter(byAge, age<60), aes(age, aware, group=1)) +
  geom_point() +
  stat_smooth(method="lm")

frq(dat$educ2)

byEd <- dat %>%
  group_by(educ2) %>%
  summarize(aware=mean(aware))

byEd

ggplot(byEd, aes(educ2, aware)) +
  geom_point() +
  stat_smooth(method="lm")

frq(aware_merg$education)

byEd2 <- aware_merg %>%
  group_by(education) %>%
  summarize(aware=mean(aware))

byEd2

ggplot(byEd2, aes(education, aware)) +
  geom_point() +
  stat_smooth(method="lm")

frq(aware_merg$female)

tab_xtab(aware_merg$female, aware_merg$aware,
         show.col.prc = T)

tab_xtab(aware_merg$aware, aware_merg$female,
         show.col.prc = T)





