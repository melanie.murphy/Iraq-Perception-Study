---
title: "Sampling tables"
output: html_document
---

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}
# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=10, fig.height=8, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=F)
source("code/00 Iraq Perception Study - prep.R")

p1 <- read_rds("data/prepared/premise/Premise International Development task.rds")
merged_premise <- read_rds("data/prepared/Iraq Perception Study - merged premise tasks.rds")
merged_dat <- read_rds("data/prepared/Iraq Perception Study - merged data.rds")
```

### Survey Sampling Across Disaggregates - Household

```{r, echo=FALSE, warning=FALSE}

loc_hh <- dat %>%
  group_by(locality) %>%
  summarize(Sample = n()) %>%
  #ungroup() %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(loc_key) %>% 
  mutate(`Disaggregate type` = loc_lab,
         Disaggregate = "Locality") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Sample, 
         Percent)

loc_hh

sex_hh <- dat %>%
  group_by(sex) %>%
  summarize(Sample = n()) %>%
  #ungroup() %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(sex_key) %>% 
  mutate(`Disaggregate type` = sex_lab,
         Disaggregate = "Sex") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Sample, 
         Percent)

sex_hh

age_hh <- dat %>%
  group_by(age_grp) %>%
  summarize(Sample = n()) %>%
  #ungroup() %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(age_key) %>% 
  mutate(`Disaggregate type` = age_grp_lab,
         Disaggregate = "Age group") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Sample, 
         Percent) %>% 
  .[-5,]

age_hh

edu_hh <- dat %>%
  group_by(education) %>%
  summarize(Sample = n()) %>%
  #ungroup() %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(educ_key) %>% 
  mutate(`Disaggregate type` = educ_lab,
         Disaggregate = "Education") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Sample, 
         Percent)

edu_hh

rel_hh <- dat %>%
  group_by(rel_grp4) %>%
  summarize(Sample = n()) %>%
  #ungroup() %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(rel_key4) %>% 
  mutate(`Disaggregate type` = rel_grp4_lab,
         Disaggregate = "Religious group") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Sample, 
         Percent)

rel_hh

gov_hh <- dat %>%
  group_by(Gov) %>%
  summarize(Sample = n()) %>%
  #ungroup() %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(gov_key) %>% 
  mutate(`Disaggregate type` = gov_lab,
         Disaggregate = "Locality") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Sample, 
         Percent)

gov_hh

```

### Governorates Sampling Across Disaggregates


```{r, echo=FALSE, warning=FALSE}

#locality - hh
gov_loc_hh <- dat %>%
  group_by(Gov, locality) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(loc_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = loc_lab,
         Disaggregate = "Locality") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_loc_hh

gov_loc_hh_tbl <- gov_loc_hh %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] 

#sex - hh
gov_sex_hh <- dat %>%
  group_by(Gov, sex) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(sex_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = sex_lab,
         Disaggregate = "Sex") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_sex_hh

gov_sex_hh_tbl <- gov_sex_hh %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] 

#age - hh
gov_age_hh <- dat %>%
  group_by(Gov, age_grp) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(age_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = age_grp_lab,
         Disaggregate = "Age group") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_age_hh

gov_age_hh_tbl <- gov_age_hh %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] %>% 
  .[-5,]

#level of education - hh
gov_edu_hh <- dat %>%
  group_by(Gov, education) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(educ_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = educ_lab,
         Disaggregate = "Education") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_edu_hh

gov_edu_hh_tbl <- gov_edu_hh %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] 

#religious group - hh
gov_rel_hh <- dat %>%
  full_join(rel_key4) %>%
  filter(is.na(rel_grp4)!=T) %>%
  group_by(Gov, factor(rel_grp4_lab), .drop = FALSE) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = `factor(rel_grp4_lab)`,
         Disaggregate = "Religious group") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_rel_hh

gov_rel_hh_tbl <- gov_rel_hh %>% 
  .[,-c(8,14)] %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] %>% 
  .[c(5,4,2,1,3),]


#inner joining to build table for gt
gov_ov_hh_tbl <- do.call(rbind, list(gov_loc_hh_tbl, gov_sex_hh_tbl, gov_age_hh_tbl, gov_edu_hh_tbl, gov_rel_hh_tbl))


#gt for all disags
gov_ov_hh_tbl_gt <- gov_ov_hh_tbl %>% 
  gt(groupname_col = "Disaggregate") %>%
  fmt_number(c(3,5,7,9,11),
             decimals=0) %>%
  fmt_percent(c(4,6,8,10,12),
              decimals=1) %>%
  cols_label(Sample_Erbil="Sample",
             `Percent_Erbil`="Percent",
             `Sample_Ninawa`="Sample",
             Percent_Ninawa="Percent",
             `Sample_Al-Anbar`="Sample",
             `Percent_Al-Anbar`="Percent",
             Sample_Baghdad="Sample",
             Percent_Baghdad="Percent",
             `Sample_Al-Basra`="Sample",
             `Percent_Al-Basra`="Percent") %>% 
  tab_spanner(columns = 3:4,
              label="Erbil") %>%
  tab_spanner(columns=5:6,
              label="Ninawa") %>%
    tab_spanner(columns = 7:8,
              label="Al-Anbar") %>%
  tab_spanner(columns=9:10,
              label="Baghdad") %>%
    tab_spanner(columns = 11:12,
              label="Al-Basra") %>% 
  tab_header("Household survey")

gov_ov_hh_tbl_gt

#gtsave(gov_ov_hh_tbl_gt, here("output/tables/sampling/gov over disags - household.rtf"))


#PREMISE
#locality - premise
gov_loc_cs <- merged_premise %>%
  group_by(Gov, locality) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(loc_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = loc_lab,
         Disaggregate = "Locality") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_loc_cs

gov_loc_cs_tbl <- gov_loc_cs %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] 

#sex - premise
gov_sex_cs <- merged_premise %>%
  group_by(Gov, sex) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(sex_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = sex_lab,
         Disaggregate = "Sex") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_sex_cs

gov_sex_cs_tbl <- gov_sex_cs %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] %>% 
  .[-3,]

#age - premise
gov_age_cs <- merged_premise %>%
  group_by(Gov, age_grp) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(age_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = age_grp_lab,
         Disaggregate = "Age group") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent)  %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_age_cs

gov_age_cs_tbl <- gov_age_cs %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] 

#level of education - premise
gov_edu_cs <- merged_premise %>%
  group_by(Gov, education) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>% 
  mutate(Percent = Sample/sum(Sample)) %>% 
  #select(-n) %>% 
  left_join(educ_key) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = educ_lab,
         Disaggregate = "Education") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent) %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_edu_cs

gov_edu_cs_tbl <- gov_edu_cs %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] 

#religious group - premise
gov_rel_cs <- merged_premise %>%
  full_join(rel_key4) %>%
  filter(is.na(rel_grp4)!=T) %>%
  group_by(Gov, factor(rel_grp4_lab), .drop = FALSE) %>%
  summarize(Sample = n()) %>%
  ungroup() %>%
  group_by(Gov) %>%
  mutate(Percent = Sample/sum(Sample)) %>% 
  left_join(gov_key) %>% 
  ungroup() %>%
  select(-Gov) %>% 
  mutate(Governorate = gov_lab,
         `Disaggregate type` = `factor(rel_grp4_lab)`,
         Disaggregate = "Religious group") %>% 
  select(Disaggregate,
         `Disaggregate type`,
         Governorate, 
         Sample, 
         Percent) %>%
  pivot_wider(names_from=Governorate,
              values_from= c(Sample, Percent))

gov_rel_cs_tbl <- gov_rel_cs %>% 
  .[c(1:3, 8, 4,9,5,10,6,11,7,12)] %>% 
  .[c(5,4,2,1,3),]

#inner joining to build table for gt
gov_ov_cs_tbl <- do.call(rbind, list(gov_loc_cs_tbl, gov_sex_cs_tbl, gov_age_cs_tbl, gov_edu_cs_tbl, gov_rel_cs_tbl))


#gt for all disags
gov_ov_cs_tbl_gt <- gov_ov_cs_tbl %>% 
  gt(groupname_col = "Disaggregate") %>%
  fmt_number(c(3,5,7,9,11),
             decimals=0) %>%
  fmt_percent(c(4,6,8,10,12),
              decimals=1) %>%
  cols_label(Sample_Erbil="Sample",
             `Percent_Erbil`="Percent",
             `Sample_Ninawa`="Sample",
             Percent_Ninawa="Percent",
             `Sample_Al-Anbar`="Sample",
             `Percent_Al-Anbar`="Percent",
             Sample_Baghdad="Sample",
             Percent_Baghdad="Percent",
             `Sample_Al-Basra`="Sample",
             `Percent_Al-Basra`="Percent") %>% 
  tab_spanner(columns = 3:4,
              label="Erbil") %>%
  tab_spanner(columns=5:6,
              label="Ninawa") %>%
    tab_spanner(columns = 7:8,
              label="Al-Anbar") %>%
  tab_spanner(columns=9:10,
              label="Baghdad") %>%
    tab_spanner(columns = 11:12,
              label="Al-Basra") %>% 
  tab_header("Crowdsource survey")

gov_ov_cs_tbl_gt

#gtsave(gov_ov_cs_tbl_gt, here("output/tables/sampling/gov over disags - premise.rtf"))


```
