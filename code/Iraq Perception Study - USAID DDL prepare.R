# Iraq Perception Study
# Devevelopment Data Library (DDL)
# Prep

source("code/00 Iraq Perception Study - prep.R")

# manually run prep file to line 46

# household raw ----

raw <- haven::read_spss("data/raw/Iraq Perception study_Jul2021-final_codedv4.sav",
                        user_na = T)

write_csv(raw, "data/raw/Iraq Perception study_Jul2021-final_codedv4.csv")
write_spss(raw, "data/raw/Iraq Perception study_Jul2021-final_codedv4.sav")



names(raw)
rawNames <- data.frame(names(raw))

frq(raw$PSU_PR)

out <- raw %>%
  group_by(PSU, Gender) %>%
  summarize(n=n()) %>%
  arrange(n)

out

psu_frq <- data.frame(frq(raw$PSU)) %>%
  select(2:3) %>%
  remove_rownames() %>%
  rename(psu_code=1, psu_lab=2)

head(psu_frq)
write_csv(psu_frq, "data/psu_key.csv")

subdistrict_frq <- data.frame(frq(raw$SubDistrict)) %>%
  select(2:3) %>%
  rename(subdistrict_code=1, subdistrict_lab=2)

subdistrict_frq

write_csv(subdistrict_frq, "data/subdistrict key.csv")

raw_pub <- raw %>%
  select(1,
         3:4,
         7:10,
         14,
         20,
         24:26,
         30:42,
         45,
         50,
         53,
         89:655) %>%
  mutate(SubDistrict_code = remove_labels(SubDistrict),
         PSU_code = remove_labels(PSU)) %>%
  relocate(SubDistrict_code, .after=SubDistrict) %>%
  relocate(PSU_code, .after=PSU) %>%
  select(-SubDistrict, -PSU)

names(raw_pub)

frq(raw_pub$PSU_code)
frq(raw_pub$SubDistrict_code)

out2 <- raw %>%
  group_by(SubDistrict, Gender, H6, H7, H8) %>%
  summarize(n=n()) %>%
  arrange(n)

out2

out3 <- raw_pub %>%
  group_by(District, Geotype, Gender, H6, H7, H8) %>%
  summarize(n=n()) %>%
  arrange(n)

out3

write_csv(out3, "output/tables/Iraq Perception Study - demographic cells.csv")

frq(dat$age_grp)

write_csv(raw_pub, "data/public/Iraq Perception Study - public use household raw.csv")
write_rds(raw_pub, "data/public/Iraq Perception Study - public use household raw.rds")
write_dta(raw_pub, "data/public/Iraq Perception Study - public use household raw.dta")
write_sav(raw_pub, "data/public/Iraq Perception Study - public use household raw.sav")

raw_pub <- read_rds(file="data/public/Iraq Perception Study - public use household raw.rds")

?read_rds

dat <- read_rds(file = "data/prepared/Iraq Perception HH Survey.rds")

names(dat)

frq(raw$District_PR)
frq(raw$SP)
frq(raw$H10)
frq(dat$Gov)

table(raw$A4, raw$Gender)
frq(raw$PSU)

# household prepared ----

prep <- read_rds(file = "data/prepared/Iraq Perception HH Survey.rds")

names(prep)
prepNames <- data.frame(names(prep))

psu_frq <- data.frame(frq(prep$PSU)) %>%
  select(2:3) %>%
  remove_rownames() %>%
  rename(psu_code=1, psu_lab=2)

subdistrict_frq <- data.frame(frq(prep$SubDistrict)) %>%
  select(2:3) %>%
  rename(subdistrict_code=1, subdistrict_lab=2)

subdistrict_frq

prep_pub <- prep %>%
  select(1,
         3:4,
         7:10,
         14,
         20,
         24:26,
         30:42,
         45,
         50,
         53,
         89:655,
         665:1001) %>%
  mutate(SubDistrict_code = remove_labels(SubDistrict),
         PSU_code = remove_labels(PSU)) %>%
  relocate(SubDistrict_code, .after=SubDistrict) %>%
  relocate(PSU_code, .after=PSU) %>%
  select(-SubDistrict, -PSU)

write_csv(prep_pub, "data/public/Iraq Perception Study - public use household prepared.csv")
write_rds(prep_pub, "data/public/Iraq Perception Study - public use household prepared.rds")
write_dta(prep_pub, "data/public/Iraq Perception Study - public use household prepared.dta")
write_sav(prep_pub, "data/public/Iraq Perception Study - public use household prepared.sav")


# premise tasks ----

p1_distinct <- read_rds("data/prepared/premise/Premise International Development task.rds")

names(p1_distinct)
view(p1_distinct)
write_csv(p1_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - International Development task.csv")
write_rds(p1_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - International Development task.rds")
write_dta(p1_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - International Development task.dta")
write_sav(p1_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - International Development task.sav")


p2_distinct <- read_rds(file="data/prepared/premise/Premise Media and Communications task - distinct with disags.rds")

write_csv(p2_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Media and Communications task.csv")
write_rds(p2_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Media and Communications task.rds")
write_dta(p2_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Media and Communications task.dta")
write_sav(p2_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Media and Communications task.sav")


p3_distinct <- read_rds(file="data/prepared/premise/Premise Development Projects task - distinct with disags.rds")

write_csv(p3_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Development Projects task.csv")
write_rds(p3_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Development Projects task.rds")
write_dta(p3_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Development Projects task.dta")
write_sav(p3_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Development Projects task.sav")


p4_distinct <- read_rds(file="data/prepared/premise/Premise Governance task - distinct.rds")

write_csv(p4_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Governance task.csv")
write_rds(p4_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Governance task.rds")
write_dta(p4_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Governance task.dta")
write_sav(p4_distinct, "data/public/Iraq Perception Study - public use crowdsource prepared - Governance task.sav")

merged_dat <- read_rds("data/prepared/Iraq Perception Study - merged data.rds")

write_csv(merged_dat, "data/public/Iraq Perception Study - public use crowdsource prepared - household crowdsource merged.csv")
write_rds(merged_dat, "data/public/Iraq Perception Study - public use crowdsource prepared - household crowdsource merged.rds")
write_dta(merged_dat, "data/public/Iraq Perception Study - public use crowdsource prepared - household crowdsource merged.dta")
write_sav(merged_dat, "data/public/Iraq Perception Study - public use crowdsource prepared - household crowdsource merged.sav")


install.packages("dataMeta")
library(dataMeta)

data(merged_dat)

data_dict(merged_dat2)

library(dataMaid)
makeCodebook(merged_dat)

makeCodebook(prep_pub,
             replace=T)

makeCodebook(prep_pub)


library(codebook)

?new_codebook_rmd

new_codebook_rmd()

str(merged_dat)

merged_dat <- merged_dat %>%
  as.data.frame()

names(merged_dat)

attributes(merged_dat$age_grp)

map_df(merged_dat, function(x) attributes(x)$label)

attributes(merged_dat$education)$label

?typeof

merg_codebook <- data.frame(ser = 1:208) %>%
  mutate(type=map_chr(merged_dat, typeof),
         Mean = map_dbl(merged_dat, mean, na.rm = T),
         Prop_miss = map_dbl(merged_dat, function(x) mean(is.na(x))))

head(merg_codebook)


merg_codebook <- map_df(merged_dat, function(x) attributes(x)$label) %>%
  gather(key = Code, value = Label)

prep_codebook <- map_df(prep_pub, function(x) attributes(x)$label)


codebook <- codebook %>%
  map_df(df, function(x) attributes(x)$label) %>%
  gather(key = Code, value = Label)
  mutate(Type = map_chr(df, typeof),
         Mean = map_dbl(df, mean, na.rm = T),
         Prop_miss = map_dbl(df, function(x) mean(is.na(x))))

codebook





