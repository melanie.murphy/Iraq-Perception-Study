---
title: "Donor Activity"
date: "2021-05-21"
output: 
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

## Introduction

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}
# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=10, fig.height=8, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=F)
source("code/00 Iraq Perception Study - prep.R")

p1_distinct <- read_rds("data/prepared/premise/Premise International Development task.rds")
p2_distinct <- read_rds("data/prepared/premise/Premise Media and Communications task - distinct with disags.rds")
p3_distinct <- read_rds("data/prepared/premise/Premise Development Projects task - distinct with disags.rds")
merged_premise <- read_rds("data/prepared/Iraq Perception Study - merged premise tasks.rds")
merged_dat <- read_rds("data/prepared/Iraq Perception Study - merged data.rds")
```

