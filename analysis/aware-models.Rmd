---
title: "Awareness of USAID"
author: "Dan Killian"
date: "2022-03-03"
output: 
  workflowr::wflow_html
      code_folding: hide
      toc: true
      toc_float: true
      toc_depth: 3
      fig.caption: true
      df_print: kable
editor_options:
  chunk_output_type: console
---

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}

# standard figure size and generate clean output
knitr::opts_chunk$set(fig.height=4, fig.width=6, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=T)

library(here)

source("code/00 Iraq Perception Study - prep.R")


```



## Introduction

# Regression models

## Linear model

## Multilevel model

## Bayesian multilevel model



```{r}

```

