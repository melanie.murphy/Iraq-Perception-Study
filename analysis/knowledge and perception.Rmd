---
title: "USAID Knowledge and Perception"
date: "2021-05-21"
output: 
  workflowr::wflow_html:
    toc: false
editor_options:
  chunk_output_type: console
---

# Introduction

```{r global_options, include=F, warning=F, message=F, echo=F, error=F}
# standard figure size and generate clean output
knitr::opts_chunk$set(fig.width=10, fig.height=8, warning=FALSE, message=FALSE, cache=TRUE, error=T, echo=F)
source("code/00 Iraq Perception Study - prep.R")

p1_distinct <- read_rds("data/prepared/premise/Premise International Development task.rds")
merged_premise <- read_rds("data/prepared/Iraq Perception Study - merged premise tasks.rds")
merged_dat <- read_rds("data/prepared/Iraq Perception Study - merged data.rds")
```


```{r, echo=FALSE, warning=FALSE}
# Functions - overall
ov_hh <- function(design, var, title) {
  design %>%
    summarise(prop=survey_mean({{var}}, na.rm=T, deff="replace"),
              total=survey_total({{var}}), sample = n()) %>%
    mutate(`Data Source`=title,
           Percent=prop,
           `Margin of error` = prop_se*1.96,
           lower=prop - `Margin of error`,
           upper=prop + `Margin of error`,
           ci=paste(round(lower,3), round(upper,3), sep="-"),
           Sample=sample) %>% 
    #ungroup() %>%
    mutate(Sample=sum(Sample, na.rm=T)) %>% 
    select(`Data Source`,
           Percent, 
           `Margin of error`,
           Sample)
  }


ov_cs <- function(data, var, source) {

  data %>%
    select({{var}}) %>%
    summarise(proportion = mean({{var}}, na.rm=T),
              se = std.error({{var}}),
              lower = proportion-2.4*se,
              upper = proportion+2.4*se,
              total = n(), sample = n()) %>%
    mutate(Percent =proportion,
           `Margin of error` = 2.4*se,
           Sample=sum(sample, na.rm=T),
           `Data Source` =source)%>%
    select(`Data Source`,
           Percent,
           `Margin of error`,
           Sample)
}

ov_merg <- function(object, var, source) {

  object %>%
    select({{var}}) %>%
    summarise(proportion = mean({{var}}, na.rm=T),
              se = std.error({{var}}),
              lower = proportion-2.4*se,
              upper = proportion+2.4*se,
              total = n(), sample = n()) %>%
    mutate(Percent =proportion,
           `Margin of error` = 2.4*se,
           Sample=sum(sample, na.rm=T),
           `Data Source` = source)%>%
    select(`Data Source`,
           Percent,
           `Margin of error`,
           Sample)
}

# Functions - disag
#function for household survey only 
disag_svyr <- function(design, var, groupvar, ind_type, key, source) {

  key <- key %>%
    rename(disag=2)

  design %>%
    filter(is.na({{groupvar}})!=T) %>% 
    group_by({{groupvar}}) %>%
    summarise(prop=survey_mean({{var}}, na.rm=T),
              sample = n()) %>%
        full_join(key) %>%
    mutate(`Data source` = source,
           Disaggregate=ind_type,
           `Disaggregate type`=disag,
           `Margin of error` = prop_se*1.96,
           lower=prop - `Margin of error`,
           upper=prop + `Margin of error`,
           ci=paste(round(lower,3), round(upper,3), sep="-"),
           Sample=sample) %>%
    mutate(Sample=sum(Sample, na.rm=T), 
           prop = ifelse(is.na(prop)==T, 0, prop), 
           `Margin of error` = ifelse(is.na(`Margin of error`)==T, 0, `Margin of error`)) %>% 
    select(`Data source`,
           Disaggregate, 
           `Disaggregate type`, 
           Percent=prop, 
           `Margin of error`, 
           Sample) 
}

#function for premise and merged
disag_cs <- function(data, var, groupvar, ind_type, key, source) {

  key <- key %>%
    rename(disag=2)

  data %>%
    group_by({{groupvar}}) %>%
    summarise(proportion = mean({{var}}, na.rm=T),
              se = std.error({{var}}),
              lower = proportion-2.4*se,
              upper = proportion+2.4*se,
              total = n(), sample = n()) %>%
    na.omit() %>%
    left_join(key) %>%
    mutate(`Data source` = source,
           Disaggregate=ind_type,
           `Disaggregate type`=disag,
           `Margin of error` = se*2.4,
           lower=proportion - `Margin of error`,
           upper=proportion + `Margin of error`,
           ci=paste(round(lower,3), round(upper,3), sep="-"),
           Sample=sum(sample, na.rm=T)) %>%
    select(`Data source`,
           Disaggregate,
      `Disaggregate type`,
      Percent=proportion,
      `Margin of error`,
      Sample)

}

```

# Dimension 1: USAID knowledge and perception
### Indicator 1: Awareness of USAID 

```{r, echo=FALSE, warning=FALSE}
#Indicator 1: Awareness of USAID

b1_hh <- ov_hh(svyrdat, aware, "Household survey")
#b1_hh
b1_cs <- ov_cs(p1_distinct, aware, "Crowdsource survey")
#b1_cs
b1_csc <- ov_cs(merged_premise, aware, "Crowdsource complete")
#b1_csc
b1_m <- ov_merg(merged_dat, aware, "Merged survey")
#b1_m

b1_all <- do.call(rbind, list(b1_hh, b1_cs, b1_csc, b1_m)) 
#b1_all

b1_all_gt <- b1_all %>%
  .[-3,] %>%
  gt() %>%
  fmt_percent(vars(Percent, `Margin of error`),
              decimals=1) %>%
  fmt_number(vars(Sample),
             decimals=0) %>% 
  tab_source_note("B1. Are you aware of the United States Agency for International Development?") 


#b1_all_gt

#write_csv(b1_all, here("output/tables/merged/indicators/aware overall - hh cs m.csv"))
#gtsave(b1_all_gt, here("output/tables/merged/indicators/aware overall - hh cs m.rtf"))

#household survey
b1_hh_loc <- disag_svyr(svyrdat, aware, locality, "Locality", loc_key, "Household survey")
b1_hh_gov <- disag_svyr(svyrdat, aware, Gov, "Governorate", gov_key, "Household survey")
b1_hh_sex <- disag_svyr(svyrdat, aware, sex, "Sex", sex_key, "Household survey")
b1_hh_age <- disag_svyr(svyrdat, aware, age_grp, "Age group", age_key, "Household survey") 
b1_hh_edu <- disag_svyr(svyrdat, aware, education, "Education level", educ_key, "Household survey") 
b1_hh_rel <- disag_svyr(svyrdat, aware, rel_grp4, "Religious group", rel_key4, "Household survey") %>% 
  .[c(1:3, 5, 4),]

#bind the household disag together - still need to add edu and rel
b1_hh_dem <- do.call(rbind, list(b1_hh_loc, b1_hh_sex, b1_hh_age, b1_hh_edu, b1_hh_rel, b1_hh_gov))
#b1_hh_dem

#premise survey
b1_cs_loc <- disag_cs(p1_distinct, aware, locality, "Locality", loc_key, "Crowdsource survey")
b1_cs_gov <- disag_cs(p1_distinct, aware, Gov, "Governorate", gov_key, "Crowdsource survey")
b1_cs_sex <- disag_cs(p1_distinct, aware, sex, "Sex", sex_key, "Crowdsource survey")
b1_cs_age <- disag_cs(p1_distinct, aware, age_grp, "Age group", age_key, "Crowdsource survey")
b1_cs_edu <- disag_cs(p1_distinct, aware, education, "Education level", educ_key, "Crowdsource survey")
b1_cs_rel <- disag_cs(p1_distinct, aware, rel_grp4, "Religious group", rel_key4, "Crowdsource survey")

#bind the premise disag together - still need to add edu and rel
b1_cs_dem <- do.call(rbind, list(b1_cs_loc, b1_cs_sex, b1_cs_age, b1_cs_edu, b1_cs_rel, b1_cs_gov))
#b1_cs_dem

#merged data disaggregates 
# b1_m_loc <- disag_merg(merged_dat, aware, locality, "Locality", loc_key)
# b1_m_gov <- disag_merg(merged_dat, aware, Gov, "Governorate", gov_key)
# b1_m_sex <- disag_merg(merged_dat, aware, sex, "Sex", sex_key)
# b1_m_age <- disag_merg(merged_dat, aware, age_grp, "Age group", age_key)
# b1_m_edu <- disag_merg(merged_dat, aware, education, "Education level", educ_key) 
# b1_m_rel <- disag_merg(merged_dat, aware, rel_grp3, "Religious group", rel_key3)
# 
# #bind the merged data disag together - still need to add edu and rel
# b1_m_dem <- do.call(rbind, list(b1_m_loc, b1_m_gov, b1_m_sex, b1_m_rel))
# b1_m_dem

#bind overall hh and premise together 
b1_all_dem <- do.call(cbind, list(b1_hh_dem, b1_cs_dem))
#b1_all_dem

b1_hh_tbl <- b1_hh %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b1_cs_tbl <- b1_cs %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b1_ov_tbl <- b1_hh_tbl %>%
  inner_join(b1_cs_tbl, by = "Disaggregate type")  %>% 
  .[,-c(1,7:8)]

b1_dem_tbl <- b1_hh_dem %>%
  inner_join(b1_cs_dem, by = "Disaggregate type") %>% 
  .[,-c(1,7:8)]

b1_all_tbl <- do.call(rbind, list(b1_ov_tbl, b1_dem_tbl))

#write_csv(b1_all_tbl, here("output/tables/merged/disag tables/aware all disag.csv"))

b1_all_tbl_gt <- b1_all_tbl %>% 
  gt(groupname_col = "Disaggregate.x") %>%
  fmt_number(c(5,8),
             decimals=0) %>%
  fmt_percent(c(3:4,6:7),
              decimals=1) %>%
  cols_label(Percent.x="Percent",
             `Margin of error.x`="Margin of error",
             `Sample.x`="Sample",
             Percent.y="Percent",
             `Margin of error.y`="Margin of error",
             Sample.y="Sample") %>% 
  tab_spanner(columns = 3:5,
              label="Household survey") %>%
  tab_spanner(columns=6:8,
              label="Crowdsource survey") %>%
  # tab_spanner(columns=9:11,
  #             label="Merged survey data") %>%
  tab_source_note(source_note="B1. Are you aware of the United States Agency for International Development?")

b1_all_tbl_gt

#gtsave(b1_all_tbl_gt, here("output/tables/merged/disag tables/aware all disag.rtf"))

```

### Indicator 2: Seen USAID logo

```{r, echo=FALSE, warning=FALSE}

#Indicator 2: Seen USAID logo

b10_hh <- ov_hh(svyrdat, seen_USAID, "Household survey")
# b10_hh
b10_cs <- ov_cs(p1_distinct, seen_USAID, "Crowdsource survey")
# b10_cs
b10_csc <- ov_cs(merged_premise, seen_USAID, "Crowdsource complete")
#b10_csc
b10_m <- ov_merg(merged_dat, seen_USAID, "Merged survey")
#b10_m

b10_all <- do.call(rbind, list(b10_hh, b10_cs, b10_csc, b10_m)) 
#b10_all

b10_all_gt <- b10_all %>%
    .[-3,] %>%
  gt() %>%
  fmt_percent(vars(Percent, `Margin of error`),
              decimals=1) %>%
  fmt_number(vars(Sample),
             decimals=0) %>% 
  tab_source_note("B10. Seen USAID logo")


#b10_all_gt

#write_csv(b10_all, here("output/tables/merged/indicators/seen logo overall - hh cs m.csv"))
#gtsave(b10_all_gt, here("output/tables/merged/indicators/seen logo overall - hh cs m.rtf"))

#household survey
b10_hh_loc <- disag_svyr(svyrdat, seen_USAID, locality, "Locality", loc_key, "Household survey")
b10_hh_gov <- disag_svyr(svyrdat, seen_USAID, Gov, "Governorate", gov_key, "Household survey")
b10_hh_sex <- disag_svyr(svyrdat, seen_USAID, sex, "Sex", sex_key, "Household survey")
b10_hh_age <- disag_svyr(svyrdat, seen_USAID, age_grp, "Age group", age_key, "Household survey") 
b10_hh_edu <- disag_svyr(svyrdat, seen_USAID, education, "Education level", educ_key, "Household survey") 
b10_hh_rel <- disag_svyr(svyrdat, seen_USAID, rel_grp4, "Religious group", rel_key4, "Household survey") %>% 
  .[c(1:3, 5, 4),]

#bind the household disag together - still need to add edu and rel
b10_hh_dem <- do.call(rbind, list(b10_hh_loc, b10_hh_sex, b10_hh_age, b10_hh_edu, b10_hh_rel, b10_hh_gov))
#b10_hh_dem

#premise survey
b10_cs_loc <- disag_cs(p1_distinct, seen_USAID, locality, "Locality", loc_key, "Crowdsource survey")
b10_cs_gov <- disag_cs(p1_distinct, seen_USAID, Gov, "Governorate", gov_key, "Crowdsource survey")
b10_cs_sex <- disag_cs(p1_distinct, seen_USAID, sex, "Sex", sex_key, "Crowdsource survey")
b10_cs_age <- disag_cs(p1_distinct, seen_USAID, age_grp, "Age group", age_key, "Crowdsource survey")
b10_cs_edu <- disag_cs(p1_distinct, seen_USAID, education, "Education level", educ_key, "Crowdsource survey")
b10_cs_rel <- disag_cs(p1_distinct, seen_USAID, rel_grp4, "Religious group", rel_key4, "Crowdsource survey")

#bind the premise disag together - still need to add edu and rel
b10_cs_dem <- do.call(rbind, list(b10_cs_loc, b10_cs_sex, b10_cs_age, b10_cs_edu, b10_cs_rel, b10_cs_gov))
#b10_cs_dem

#merged data disaggregates 
# b10_m_loc <- disag_merg(merged_dat, seen_USAID, locality, "Locality", loc_key)
# b10_m_gov <- disag_merg(merged_dat, seen_USAID, Gov, "Governorate", gov_key)
# b10_m_sex <- disag_merg(merged_dat, seen_USAID, sex, "Sex", sex_key)
# b10_m_age <- disag_merg(merged_dat, seen_USAID, age_grp, "Age group", age_key)
# b10_m_edu <- disag_merg(merged_dat, seen_USAID, education, "Education level", educ_key) 
# b10_m_rel <- disag_merg(merged_dat, seen_USAID, rel_grp3, "Religious group", rel_key3)
# 
# #bind the merged data disag together - still need to add edu and rel
# b10_m_dem <- do.call(rbind, list(b10_m_loc, b10_m_gov, b10_m_sex, b10_m_rel))
# b10_m_dem

#bind overall hh and premise together 
b10_all_dem <- do.call(cbind, list(b10_hh_dem, b10_cs_dem))
#b10_all_dem

b10_hh_tbl <- b10_hh %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b10_cs_tbl <- b10_cs %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b10_ov_tbl <- b10_hh_tbl %>%
  inner_join(b10_cs_tbl, by = "Disaggregate type")  %>% 
  .[,-c(1,7:8)]

b10_dem_tbl <- b10_hh_dem %>%
  inner_join(b10_cs_dem, by = "Disaggregate type") %>% 
  .[,-c(1,7:8)]

b10_all_tbl <- do.call(rbind, list(b10_ov_tbl, b10_dem_tbl))

#write_csv(b10_all_tbl, here("output/tables/merged/disag tables/seen USAID logo disag.csv"))

b10_all_tbl_gt <- b10_all_tbl %>% 
  gt(groupname_col = "Disaggregate.x") %>%
  fmt_number(c(5,8),
             decimals=0) %>%
  fmt_percent(c(3:4,6:7),
              decimals=1) %>%
  cols_label(Percent.x="Percent",
             `Margin of error.x`="Margin of error",
             `Sample.x`="Sample",
             Percent.y="Percent",
             `Margin of error.y`="Margin of error",
             Sample.y="Sample") %>% 
  tab_spanner(columns = 3:5,
              label="Household survey") %>%
  tab_spanner(columns=6:8,
              label="Crowdsource survey") %>%
  # tab_spanner(columns=9:11,
  #             label="Merged survey data") %>%
  tab_source_note(source_note="B10. Seen USAID logo")

b10_all_tbl_gt

#gtsave(b10_all_tbl_gt, here("output/tables/merged/disag tables/seen USAID logo disag.rtf"))

```

### Indicator 3: Recognize USAID logo

```{r, echo=FALSE, warning=FALSE}

#Indicator 3: Recognize USAID logo - waiting for export to be fixed

b11_hh <- ov_hh(svyrdat, recognize_USAID, "Household survey")
# b11_hh
# b11_cs <- ov_cs(p1_distinct, recognize_USAID, "Crowdsource survey")
# # b11_cs
# b11_csc <- ov_cs(merged_premise, recognize_USAID, "Crowdsource complete")
# #b11_csc
# b11_m <- ov_merg(merged_dat, recognize_USAID, "Merged survey")
# #b11_m
# 
# b11_all <- do.call(rbind, list(b11_hh, b11_cs, b11_csc, b11_m)) 
# #b10_all
# 
# b11_all_gt <- b11_all %>%
#     .[-3,] %>%
#   gt() %>%
#   fmt_percent(vars(Percent, `Margin of error`),
#               decimals=1) %>%
#   fmt_number(vars(Sample),
#              decimals=0) %>% 
#   tab_source_note("B11. Recognize USAID logo")
# 
# 
# b11_all_gt

#write_csv(b10_all, here("output/tables/merged/indicators/recognize logo overall - hh cs m.csv"))
#gtsave(b10_all_gt, here("output/tables/merged/indicators/recognize logo overall - hh cs m.rtf"))

#household survey
b11_hh_loc <- disag_svyr(svyrdat, recognize_USAID, locality, "Locality", loc_key, "Household survey")
b11_hh_gov <- disag_svyr(svyrdat, recognize_USAID, Gov, "Governorate", gov_key, "Household survey")
b11_hh_sex <- disag_svyr(svyrdat, recognize_USAID, sex, "Sex", sex_key, "Household survey")
b11_hh_age <- disag_svyr(svyrdat, recognize_USAID, age_grp, "Age group", age_key, "Household survey")
b11_hh_edu <- disag_svyr(svyrdat, recognize_USAID, education, "Education level", educ_key, "Household survey")
b11_hh_rel <- disag_svyr(svyrdat, recognize_USAID, rel_grp4, "Religious group", rel_key4, "Household survey") %>%
  .[c(1:3, 5, 4),]

#bind the household disag together - still need to add edu and rel
b11_hh_dem <- do.call(rbind, list(b11_hh_loc, b11_hh_sex, b11_hh_age, b11_hh_edu, b11_hh_rel, b11_hh_gov))
#b11_hh_dem

#premise survey
# b11_cs_loc <- disag_cs(p1_distinct, recognize_USAID, locality, "Locality", loc_key, "Crowdsource survey")
# b11_cs_gov <- disag_cs(p1_distinct, recognize_USAID, Gov, "Governorate", gov_key, "Crowdsource survey")
# b11_cs_sex <- disag_cs(p1_distinct, recognize_USAID, sex, "Sex", sex_key, "Crowdsource survey")
# b11_cs_age <- disag_cs(p1_distinct, recognize_USAID, age_grp, "Age group", age_key, "Crowdsource survey")
# b11_cs_edu <- disag_cs(p1_distinct, recognize_USAID, education, "Education level", educ_key, "Crowdsource survey")
# b11_cs_rel <- disag_cs(p1_distinct, recognize_USAID, rel_grp4, "Religious group", rel_key4, "Crowdsource survey")
# 
# #bind the premise disag together - still need to add edu and rel
# b11_cs_dem <- do.call(rbind, list(b11_cs_loc, b11_cs_sex, b11_cs_age, b11_cs_edu, b11_cs_rel, b11_cs_gov))
# b11_cs_dem

#merged data disaggregates
# b11_m_loc <- disag_merg(merged_dat, recognize_USAID, locality, "Locality", loc_key)
# b11_m_gov <- disag_merg(merged_dat, recognize_USAID, Gov, "Governorate", gov_key)
# b11_m_sex <- disag_merg(merged_dat, recognize_USAID, sex, "Sex", sex_key)
# b11_m_age <- disag_merg(merged_dat, recognize_USAID, age_grp, "Age group", age_key)
# b11_m_edu <- disag_merg(merged_dat, recognize_USAID, education, "Education level", educ_key)
# b11_m_rel <- disag_merg(merged_dat, recognize_USAID, rel_grp3, "Religious group", rel_key3)
#
# #bind the merged data disag together - still need to add edu and rel
# b11_m_dem <- do.call(rbind, list(b11_m_loc, b11_m_gov, b11_m_sex, b11_m_rel))
# b11_m_dem

#bind overall hh and premise together
# b11_all_dem <- do.call(cbind, list(b11_hh_dem, b11_cs_dem))
# b11_all_dem

b11_ov_tbl <- b11_hh %>%
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>%
  relocate(Disaggregate, .after = `Data Source`) %>%
  relocate(`Disaggregate type`, .after = Disaggregate) %>% 
  .[,-1]

b11_dem_tbl <- b11_hh_dem %>% 
  .[,-1]

b11_all_tbl <- do.call(rbind, list(b11_ov_tbl, b11_dem_tbl))

#write_csv(b11_all_tbl, here("output/tables/merged/disag tables/recognize USAID disag.csv"))

b11_all_tbl_gt <- b11_all_tbl %>%
  gt(groupname_col = "Disaggregate") %>%
  fmt_number(c(5),
             decimals=0) %>%
  fmt_percent(c(3:4),
              decimals=1) %>%
  tab_spanner(columns = 3:5,
              label="Household survey") %>%
  tab_source_note(source_note="B11. Recognize USAID logo")

b11_all_tbl_gt

#gtsave(b11_all_tbl_gt, here("output/tables/merged/disag tables/recognize USAID  disag.rtf"))

```

### Indicator 4: Positive perception of USAID

```{r, echo=FALSE, warning=FALSE}

#Indicator 4: Positive perception of USAID

b3_hh <- ov_hh(svyrdat, percept_bin, "Household survey")
# b3_hh
b3_cs <- ov_cs(p1_distinct, percept_bin, "Crowdsource survey")
# b3_cs
b3_csc <- ov_cs(merged_premise, percept_bin, "Crowdsource complete")
#b3_csc
b3_m <- ov_merg(merged_dat, percept_bin, "Merged survey")
#b3_m

b3_all <- do.call(rbind, list(b3_hh, b3_cs, b3_csc, b3_m)) 
#b3_all

b3_all_gt <- b3_all %>%
    .[-3,] %>%
  gt() %>%
  fmt_percent(vars(Percent, `Margin of error`),
              decimals=1) %>%
  fmt_number(vars(Sample),
             decimals=0) %>% 
  tab_source_note("B3. Would you say your impression of USAID is positive or negative?Conditional on reporting awareness of USAID")

#b3_all_gt

#write_csv(b3_all, here("output/tables/merged/indicators/USAID perception overall - hh cs m.csv"))
#gtsave(b3_all_gt, here("output/tables/merged/indicators/USAID perception overall - hh cs m.rtf"))

#household survey
b3_hh_loc <- disag_svyr(svyrdat, percept_bin, locality, "Locality", loc_key, "Household survey")
b3_hh_gov <- disag_svyr(svyrdat, percept_bin, Gov, "Governorate", gov_key, "Household survey")
b3_hh_sex <- disag_svyr(svyrdat, percept_bin, sex, "Sex", sex_key, "Household survey")
b3_hh_age <- disag_svyr(svyrdat, percept_bin, age_grp, "Age group", age_key, "Household survey") 
b3_hh_edu <- disag_svyr(svyrdat, percept_bin, education, "Education level", educ_key, "Household survey") 
b3_hh_rel <- disag_svyr(svyrdat, percept_bin, rel_grp4, "Religious group", rel_key4, "Household survey") %>% 
  .[c(1:3, 5, 4),]

#bind the household disag together - still need to add edu and rel
b3_hh_dem <- do.call(rbind, list(b3_hh_loc, b3_hh_sex, b3_hh_age, b3_hh_edu, b3_hh_rel, b3_hh_gov))
#b3_hh_dem

#premise survey
b3_cs_loc <- disag_cs(p1_distinct, percept_bin, locality, "Locality", loc_key, "Crowdsource survey")
b3_cs_gov <- disag_cs(p1_distinct, percept_bin, Gov, "Governorate", gov_key, "Crowdsource survey")
b3_cs_sex <- disag_cs(p1_distinct, percept_bin, sex, "Sex", sex_key, "Crowdsource survey")
b3_cs_age <- disag_cs(p1_distinct, percept_bin, age_grp, "Age group", age_key, "Crowdsource survey")
b3_cs_edu <- disag_cs(p1_distinct, percept_bin, education, "Education level", educ_key, "Crowdsource survey")
b3_cs_rel <- disag_cs(p1_distinct, percept_bin, rel_grp4, "Religious group", rel_key4, "Crowdsource survey")

#bind the premise disag together - still need to add edu and rel
b3_cs_dem <- do.call(rbind, list(b3_cs_loc, b3_cs_sex, b3_cs_age, b3_cs_edu, b3_cs_rel, b3_cs_gov))
#b3_cs_dem

#merged data disaggregates 
# b3_m_loc <- disag_merg(merged_dat, percept_bin, locality, "Locality", loc_key)
# b3_m_gov <- disag_merg(merged_dat, percept_bin, Gov, "Governorate", gov_key)
# b3_m_sex <- disag_merg(merged_dat, percept_bin, sex, "Sex", sex_key)
# b3_m_age <- disag_merg(merged_dat, percept_bin, age_grp, "Age group", age_key)
# b3_m_edu <- disag_merg(merged_dat, percept_bin, education, "Education level", educ_key) 
# b3_m_rel <- disag_merg(merged_dat, percept_bin, rel_grp3, "Religious group", rel_key3)
# 
# #bind the merged data disag together - still need to add edu and rel
# b3_m_dem <- do.call(rbind, list(b3_m_loc, b3_m_gov, b3_m_sex, b3_m_rel))
# b3_m_dem

#bind overall hh and premise together 
b3_all_dem <- do.call(cbind, list(b3_hh_dem, b3_cs_dem))
#b3_all_dem

b3_hh_tbl <- b3_hh %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b3_cs_tbl <- b3_cs %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b3_ov_tbl <- b3_hh_tbl %>%
  inner_join(b3_cs_tbl, by = "Disaggregate type")  %>% 
  .[,-c(1,7:8)]

b3_dem_tbl <- b3_hh_dem %>%
  inner_join(b3_cs_dem, by = "Disaggregate type") %>% 
  .[,-c(1,7:8)]

b3_all_tbl <- do.call(rbind, list(b3_ov_tbl, b3_dem_tbl))

#write_csv(b3_all_tbl, here("output/tables/merged/disag tables/percept of USAID disag.csv"))

b3_all_tbl_gt <- b3_all_tbl %>% 
  gt(groupname_col = "Disaggregate.x") %>%
  fmt_number(c(5,8),
             decimals=0) %>%
  fmt_percent(c(3:4,6:7),
              decimals=1) %>%
  cols_label(Percent.x="Percent",
             `Margin of error.x`="Margin of error",
             `Sample.x`="Sample",
             Percent.y="Percent",
             `Margin of error.y`="Margin of error",
             Sample.y="Sample") %>% 
  tab_spanner(columns = 3:5,
              label="Household survey") %>%
  tab_spanner(columns=6:8,
              label="Crowdsource survey") %>%
  # tab_spanner(columns=9:11,
  #             label="Merged survey data") %>%
  tab_source_note(source_note="B3. Would you say your impression of USAID is positive or negative? Conditional on reporting awareness of USAID")

b3_all_tbl_gt

#gtsave(b3_all_tbl_gt, here("output/tables/merged/disag tables/percept of USAID disag.rtf"))

```

### Indicator 5: Improved perception of USAID

```{r, echo=FALSE, warning=FALSE}

#Indicator 5: Improved perception of USAID

b6_hh <- ov_hh(svyrdat, improve_percept, "Household survey")
# b6_hh
b6_cs <- ov_cs(p1_distinct, improve_percept, "Crowdsource survey")
# b6_cs
b6_csc <- ov_cs(merged_premise, improve_percept, "Crowdsource complete")
#b6_csc
b6_m <- ov_merg(merged_dat, improve_percept, "Merged survey")
#b6_m

b6_all <- do.call(rbind, list(b6_hh, b6_cs, b6_csc, b6_m)) 
#b6_all

b6_all_gt <- b6_all %>%
    .[-3,] %>%
  gt() %>%
  fmt_percent(vars(Percent, `Margin of error`),
              decimals=1) %>%
  fmt_number(vars(Sample),
             decimals=0) %>% 
  tab_source_note("Improved perception of USAID")


#b6_all_gt

#write_csv(b6_all, here("output/tables/merged/indicators/improved perception overall - hh cs m.csv"))
#gtsave(b6_all_gt, here("output/tables/merged/indicators/improved perception overall - hh cs m.rtf"))

#household survey
b6_hh_loc <- disag_svyr(svyrdat, improve_percept, locality, "Locality", loc_key, "Household survey")
b6_hh_gov <- disag_svyr(svyrdat, improve_percept, Gov, "Governorate", gov_key, "Household survey")
b6_hh_sex <- disag_svyr(svyrdat, improve_percept, sex, "Sex", sex_key, "Household survey")
b6_hh_age <- disag_svyr(svyrdat, improve_percept, age_grp, "Age group", age_key, "Household survey") 
b6_hh_edu <- disag_svyr(svyrdat, improve_percept, education, "Education level", educ_key, "Household survey") 
b6_hh_rel <- disag_svyr(svyrdat, improve_percept, rel_grp4, "Religious group", rel_key4, "Household survey") %>% 
  .[c(1:3, 5, 4),]

#bind the household disag together - still need to add edu and rel
b6_hh_dem <- do.call(rbind, list(b6_hh_loc, b6_hh_sex, b6_hh_age, b6_hh_edu, b6_hh_rel, b6_hh_gov))
#b6_hh_dem

#premise survey
b6_cs_loc <- disag_cs(p1_distinct, improve_percept, locality, "Locality", loc_key, "Crowdsource survey")
b6_cs_gov <- disag_cs(p1_distinct, improve_percept, Gov, "Governorate", gov_key, "Crowdsource survey")
b6_cs_sex <- disag_cs(p1_distinct, improve_percept, sex, "Sex", sex_key, "Crowdsource survey")
b6_cs_age <- disag_cs(p1_distinct, improve_percept, age_grp, "Age group", age_key, "Crowdsource survey")
b6_cs_edu <- disag_cs(p1_distinct, improve_percept, education, "Education level", educ_key, "Crowdsource survey")
b6_cs_rel <- disag_cs(p1_distinct, improve_percept, rel_grp4, "Religious group", rel_key4, "Crowdsource survey")

#bind the premise disag together - still need to add edu and rel
b6_cs_dem <- do.call(rbind, list(b6_cs_loc, b6_cs_sex, b6_cs_age, b6_cs_edu, b6_cs_rel, b6_cs_gov))
#b6_cs_dem

#merged data disaggregates 
# b6_m_loc <- disag_merg(merged_dat, improve_percept, locality, "Locality", loc_key)
# b6_m_gov <- disag_merg(merged_dat, improve_percept, Gov, "Governorate", gov_key)
# b6_m_sex <- disag_merg(merged_dat, improve_percept, sex, "Sex", sex_key)
# b6_m_age <- disag_merg(merged_dat, improve_percept, age_grp, "Age group", age_key)
# b6_m_edu <- disag_merg(merged_dat, improve_percept, education, "Education level", educ_key) 
# b6_m_rel <- disag_merg(merged_dat, improve_percept, rel_grp3, "Religious group", rel_key3)
# 
# #bind the merged data disag together - still need to add edu and rel
# b6_m_dem <- do.call(rbind, list(b6_m_loc, b6_m_gov, b6_m_sex, b6_m_rel))
# b6_m_dem

#bind overall hh and premise together 
b6_all_dem <- do.call(cbind, list(b6_hh_dem, b6_cs_dem))
#b6_all_dem

b6_hh_tbl <- b6_hh %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b6_cs_tbl <- b6_cs %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b6_ov_tbl <- b6_hh_tbl %>%
  inner_join(b6_cs_tbl, by = "Disaggregate type")  %>% 
  .[,-c(1,7:8)]

b6_dem_tbl <- b6_hh_dem %>%
  inner_join(b6_cs_dem, by = "Disaggregate type") %>% 
  .[,-c(1,7:8)]

b6_all_tbl <- do.call(rbind, list(b6_ov_tbl, b6_dem_tbl))

#write_csv(b6_all_tbl, here("output/tables/merged/disag tables/improved percept disag.csv"))

b6_all_tbl_gt <- b6_all_tbl %>% 
  gt(groupname_col = "Disaggregate.x") %>%
  fmt_number(c(5,8),
             decimals=0) %>%
  fmt_percent(c(3:4,6:7),
              decimals=1) %>%
  cols_label(Percent.x="Percent",
             `Margin of error.x`="Margin of error",
             `Sample.x`="Sample",
             Percent.y="Percent",
             `Margin of error.y`="Margin of error",
             Sample.y="Sample") %>% 
  tab_spanner(columns = 3:5,
              label="Household survey") %>%
  tab_spanner(columns=6:8,
              label="Crowdsource survey") %>%
  # tab_spanner(columns=9:11,
  #             label="Merged survey data") %>%
  tab_source_note(source_note="Improved perception of USAID")

b6_all_tbl_gt

#gtsave(b6_all_tbl_gt, here("output/tables/merged/disag tables/improved percept disag.rtf"))

```

### Indicator 6: Positive perception of USAID performance

```{r, echo=FALSE, warning=FALSE}

#Indicator 6: Positive perception of USAID performance

b9_hh <- ov_hh(svyrdat, USAID_performance, "Household survey")
# b9_hh
b9_cs <- ov_cs(p1_distinct, USAID_performance, "Crowdsource survey")
# b9_cs
b9_csc <- ov_cs(merged_premise, USAID_performance, "Crowdsource complete")
#b9_csc
b9_m <- ov_merg(merged_dat, USAID_performance, "Merged survey")
#b9_m

b9_all <- do.call(rbind, list(b9_hh, b9_cs, b9_csc, b9_m)) 
#b9_all

b9_all_gt <- b9_all %>%
    .[-3,] %>%
  gt() %>%
  fmt_percent(vars(Percent, `Margin of error`),
              decimals=1) %>%
  fmt_number(vars(Sample),
             decimals=0) %>% 
  tab_source_note("B9. Do you think that overall USAID is doing a bad job or a good job in Iraq?")


#b9_all_gt

#write_csv(b9_all, here("output/tables/merged/indicators/USAID performance overall - hh cs m.csv"))
#gtsave(b9_all_gt, here("output/tables/merged/indicators/USAID performance overall - hh cs m.rtf"))

#household survey
b9_hh_loc <- disag_svyr(svyrdat, USAID_performance, locality, "Locality", loc_key, "Household survey")
b9_hh_gov <- disag_svyr(svyrdat, USAID_performance, Gov, "Governorate", gov_key, "Household survey")
b9_hh_sex <- disag_svyr(svyrdat, USAID_performance, sex, "Sex", sex_key, "Household survey")
b9_hh_age <- disag_svyr(svyrdat, USAID_performance, age_grp, "Age group", age_key, "Household survey") 
b9_hh_edu <- disag_svyr(svyrdat, USAID_performance, education, "Education level", educ_key, "Household survey") 
b9_hh_rel <- disag_svyr(svyrdat, USAID_performance, rel_grp4, "Religious group", rel_key4, "Household survey") %>% 
  .[c(1:3, 5, 4),]

#bind the household disag together - still need to add edu and rel
b9_hh_dem <- do.call(rbind, list(b9_hh_loc, b9_hh_sex, b9_hh_age, b9_hh_edu, b9_hh_rel, b9_hh_gov))
#b9_hh_dem

#premise survey
b9_cs_loc <- disag_cs(p1_distinct, USAID_performance, locality, "Locality", loc_key, "Crowdsource survey")
b9_cs_gov <- disag_cs(p1_distinct, USAID_performance, Gov, "Governorate", gov_key, "Crowdsource survey")
b9_cs_sex <- disag_cs(p1_distinct, USAID_performance, sex, "Sex", sex_key, "Crowdsource survey")
b9_cs_age <- disag_cs(p1_distinct, USAID_performance, age_grp, "Age group", age_key, "Crowdsource survey")
b9_cs_edu <- disag_cs(p1_distinct, USAID_performance, education, "Education level", educ_key, "Crowdsource survey")
b9_cs_rel <- disag_cs(p1_distinct, USAID_performance, rel_grp4, "Religious group", rel_key4, "Crowdsource survey")

#bind the premise disag together - still need to add edu and rel
b9_cs_dem <- do.call(rbind, list(b9_cs_loc, b9_cs_sex, b9_cs_age, b9_cs_edu, b9_cs_rel, b9_cs_gov))
#b9_cs_dem

#merged data disaggregates 
# b9_m_loc <- disag_merg(merged_dat, USAID_performance, locality, "Locality", loc_key)
# b9_m_gov <- disag_merg(merged_dat, USAID_performance, Gov, "Governorate", gov_key)
# b9_m_sex <- disag_merg(merged_dat, USAID_performance, sex, "Sex", sex_key)
# b9_m_age <- disag_merg(merged_dat, USAID_performance, age_grp, "Age group", age_key)
# b9_m_edu <- disag_merg(merged_dat, USAID_performance, education, "Education level", educ_key) 
# b9_m_rel <- disag_merg(merged_dat, USAID_performance, rel_grp3, "Religious group", rel_key3)
# 
# #bind the merged data disag together - still need to add edu and rel
# b9_m_dem <- do.call(rbind, list(b9_m_loc, b9_m_gov, b9_m_sex, b9_m_rel))
# b9_m_dem

#bind overall hh and premise together 
b9_all_dem <- do.call(cbind, list(b9_hh_dem, b9_cs_dem))
#b9_all_dem

b9_hh_tbl <- b9_hh %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b9_cs_tbl <- b9_cs %>% 
  mutate(Disaggregate="Overall",
         `Disaggregate type` = "Overall") %>% 
  relocate(Disaggregate, .after = `Data Source`) %>% 
  relocate(`Disaggregate type`, .after = Disaggregate)

b9_ov_tbl <- b9_hh_tbl %>%
  inner_join(b9_cs_tbl, by = "Disaggregate type")  %>% 
  .[,-c(1,7:8)]

b9_dem_tbl <- b9_hh_dem %>%
  inner_join(b9_cs_dem, by = "Disaggregate type") %>% 
  .[,-c(1,7:8)]

b9_all_tbl <- do.call(rbind, list(b9_ov_tbl, b9_dem_tbl))

#write_csv(b9_all_tbl, here("output/tables/merged/disag tables/USAID performance disag.csv"))

b9_all_tbl_gt <- b9_all_tbl %>% 
  gt(groupname_col = "Disaggregate.x") %>%
  fmt_number(c(5,8),
             decimals=0) %>%
  fmt_percent(c(3:4,6:7),
              decimals=1) %>%
  cols_label(Percent.x="Percent",
             `Margin of error.x`="Margin of error",
             `Sample.x`="Sample",
             Percent.y="Percent",
             `Margin of error.y`="Margin of error",
             Sample.y="Sample") %>% 
  tab_spanner(columns = 3:5,
              label="Household survey") %>%
  tab_spanner(columns=6:8,
              label="Crowdsource survey") %>%
  # tab_spanner(columns=9:11,
  #             label="Merged survey data") %>%
  tab_source_note(source_note="B9. Do you think that overall USAID is doing a bad job or a good job in Iraq?")

b9_all_tbl_gt

#gtsave(b9_all_tbl_gt, here("output/tables/merged/disag tables/USAID performance disag.rtf"))

```

